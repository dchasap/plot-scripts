#!/usr/bin/python

import argparse
import pandas as pd
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-files", dest="input_files", nargs='*', required=True, help="Input data file to be plotted")
parser.add_argument("--labels", dest="labels", nargs="*", help="Labels for the corresponding input files")
parser.add_argument("-o", "--output-file", dest="output_file", default="out.pdf", help="Output figure")
parser.add_argument("-y", "--ydata", dest="ydata", nargs='*', required=True, help="Data to plot on y axis")
parser.add_argument("-x", "--xdata", dest="xdata", default='NA', help="Data to plot on x axis")
parser.add_argument("--xlabel", dest="xlabel", default='x', help="Label for x axis")
parser.add_argument("--ylabel", dest="ylabel", default='y', help="Label for y axis")
parser.add_argument("--xticks", dest="xticks", action="store_true", help="Show x ticks")
parser.add_argument("--yticks", dest="yticks", action="store_true", help="Show y ticks")
parser.add_argument("--title", dest="title", default='', help="Plot title")
parser.add_argument("--style-xkcd", dest="style_xkcd_enabled", action="store_true", help="Create an xkcd style plot")
parser.add_argument("--remove-legend", dest="legend_enabled", action="store_false", help="Remove legend from figure")
parser.add_argument("--xaxis_percentages", dest="xaxis_percentages", action="store_true", help="Set x axis tick labels to percentages")
parser.add_argument("--custom-color-palette", dest="custom_color_palette", nargs="*", required=False, help="Define a custom color palette (exptects hex values seperated by spaces)")
parser.add_argument("--choose-color-palette", dest="color_palette", default="default", help="Choose a predefined color palette (default, greyscale, metro_ui, program)")


args = parser.parse_args()


#init figure
if args.style_xkcd_enabled:
	plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
if not args.xticks:
	plt.xticks([])
if not args.yticks:
	plt.yticks([])
plt.xlabel(args.xlabel)
plt.ylabel(args.ylabel)
plt.title(args.title)
#choose color palette
color_palettes = {	"default" : ["b", "r", "g", "y", "m"],
										"greyscale" : ["#000000", "#7b7d7b", "#969696", "#bfbfbf", "#ebebeb"],
										"metro_ui" : ["#00aedb", "#d11141", "#00b159", "#ffc425", "#f37735"],
										"program"	: ["#00a0b0", "#cc2a36", "#4f372d", "#edc951", "#eb6841"],
}

if args.custom_color_palette != None:
	colors=args.custom_color_palette
else:
	colors=color_palettes[args.color_palette]

#plot data from files
line_no = 0
for input_file in args.input_files:

	df = pd.read_csv(input_file)

	for y in args.ydata:
		
		ydata = df[y]
		if args.xdata != "NA":
			xdata = df[args.xdata]
		else:
			xdata = range(0, len(ydata)) 
		if args.labels is not None:
			label = args.labels.pop(0)
		else:
			label = "line " + str(line_no)

		plt.plot(xdata, ydata, label=label)

		line_no += 1

if args.legend_enabled and line_no > 1:
	plt.legend(loc="best")

if args.xaxis_percentages:
	vals = ax.get_yticks()
	ax.set_yticklabels(['{:3.2f}%'.format(x) for x in vals])

#ax.set_ylim([-30, 10])
#plt.semilogx()
#plt.semilogy()
#plt.xlim()
plt.savefig(args.output_file)
