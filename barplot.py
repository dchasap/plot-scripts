#!/usr/bin/python

import argparse
import pandas as pd
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-files", dest="input_files", nargs='*', required=True, help="Input data file to be plotted")
parser.add_argument("--labels", dest="labels", nargs="*", help="Labels for the corresponding input files")
parser.add_argument("-o", "--output-file", dest="output_file", default="out.pdf", help="Output figure")
parser.add_argument("-y", "--ydata", dest="ydata", nargs='*', required=True, help="Data to plot on y axis")
parser.add_argument("-x", "--xdata", dest="xdata", default='NA', help="Data to plot on x axis")
parser.add_argument("--xlabel", dest="xlabel", default='x', help="Label for x axis")
parser.add_argument("--ylabel", dest="ylabel", default='y', help="Label for y axis")
parser.add_argument("--title", dest="title", default='', help="Plot title")
parser.add_argument("--bar-width", dest="bar_width", default=0.8, help="Bar width")
parser.add_argument("--line-width", dest="line_width", default=1, help="Line width")
parser.add_argument("--point-size", dest="point_size", default=5, help="Point size")
parser.add_argument("--style-xkcd", dest="style_xkcd_enabled", action="store_true", help="Create an xkcd style plot")
parser.add_argument("--remove-legend", dest="legend_enabled", action="store_false", help="Remove legend from figure")
parser.add_argument("--legend-position", dest="legend_position", default="best", help="Define legend position")
parser.add_argument("--stacked", dest="stacked", action="store_true", help="Stack multiple bars (clustered by default)")
parser.add_argument("--xaxis-percentages", dest="xaxis_percentages", action="store_true", help="Set x axis tick labels to percentages")
parser.add_argument("--custom-color-palette", dest="custom_color_palette", nargs="*", required=False, help="Define a custom color palette (exptects hex values seperated by spaces)")
parser.add_argument("--choose-color-palette", dest="color_palette", default="default", help="Choose a predefined color palette (default, greyscale, metro_ui, program)")
parser.add_argument("--disable-xticks", dest="disable_xticks", action="store_true", help="Do not show xticks")
parser.add_argument("--barplot", dest="barplot", action="store_false", help="Draw a bar plot (enabled by defaul)")
parser.add_argument("--scatterplot", dest="scatterplot", action="store_true", help="Draw a scatter plot")
parser.add_argument("--lineplot", dest="lineplot", action="store_true", help="Draw a line plot")

args = parser.parse_args()


#init figure
if args.style_xkcd_enabled:
	plt.xkcd()

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()
ax.set_axisbelow(True)
ax.grid(b=True, which='major', linestyle='--')
#plt.xticks([], [])
#plt.yticks([])
#plt.xticks(rotation=90)
plt.xlabel(args.xlabel)
plt.ylabel(args.ylabel)
plt.title(args.title)

#choose color palette
color_palettes = {	"default" : ["b", "r", "g", "m", "y"],
										"greyscale" : ["#000000", "#7b7d7b", "#969696", "#bfbfbf", "#ebebeb"],
										"metro_ui" : ["#00aedb", "#d11141", "#00b159", "#ffc425", "#f37735"],
										"program"	: ["#00a0b0", "#cc2a36", "#4f372d", "#edc951", "#eb6841"],
}

markers=["o", "^", "s", "v", "d"]
linestyles=["-", "--", "-.", ":"]

if args.custom_color_palette != None:
	colors=args.custom_color_palette
else:
	colors=color_palettes[args.color_palette]

line_no = 0
for input_file in args.input_files:

	df = pd.read_csv(input_file)
	
	for y in args.ydata:
		
		ydata = df[y]
		if args.xdata != "NA":
			xdata = df[args.xdata]
			if not args.lineplot:
				plt.xticks(range(len(xdata)), xdata)
		else:
			xdata = range(0, len(ydata)) 
			plt.xticks([])

		if args.labels is not None:
			label = args.labels.pop(0)
		else:
			label = "line " + str(line_no)

		if args.scatterplot:
			marker = markers[line_no % len(markers)]
			size=float(args.point_size)
		else:
			#marker = "-"
			linestyle = linestyles[line_no % len(linestyles)]
			size=float(args.line_width)

		if line_no == 0:
			if args.scatterplot:
				ax.plot(range(len(ydata)), ydata, marker, markersize=size, label=label, color=colors.pop(0))
			elif args.lineplot:
				color = colors.pop(0)
				ax.plot(xdata, ydata, linestyle, linewidth=size, markersize=size, label=label, color=color)
			else:
				ax.bar(range(len(ydata)), ydata, width=float(args.bar_width), align='center', label=label, color=colors.pop(0))
			stacked_ydata = ydata
		elif args.stacked:
			if args.scatterplot:
				ax.plot(range(len(ydata)), ydata, marker, markersize=size, label=label, color=colors.pop(0))
			elif args.lineplot:	
				color = colors.pop(0)
				ax.plot(xdata, ydata, linestyle, linewidth=size, markersize=size, label=label, color=color)
			else:
				ax.bar(range(len(ydata)), ydata, width=float(args.bar_width), align='center', label=label, color=colors.pop(0), bottom=stacked_ydata)
			stacked_ydata += ydata
		else:
			if args.scatterplot:
				ax.plot(range(len(ydata)), ydata, marker, markersize=size, label=label, color=colors.pop(0))
			elif args.lineplot:	
				color = colors.pop(0)
				ax.plot(xdata, ydata, linestyle, linewidth=size, markersize=size, label=label, color=color)
			else:
				ax.bar(range(len(ydata)), ydata, width=float(args.bar_width), label=label, color=colors.pop(0))
		
		line_no += 1

if args.disable_xticks:
	plt.xticks([], [])

if args.legend_enabled and line_no > 1:
	plt.legend(loc=args.legend_position, prop={'size':10})

if args.xaxis_percentages:
	vals = ax.get_yticks()
	ax.set_yticklabels(['{:3.2f}%'.format(x) for x in vals])

#ax.set_ylim([-30, 10])
#plt.semilogx()
#plt.semilogy()
#plt.xlim()
plt.savefig(args.output_file)
